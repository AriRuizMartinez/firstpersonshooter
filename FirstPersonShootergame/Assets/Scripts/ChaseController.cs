using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseController : MonoBehaviour
{
    [SerializeField] private GameObject enemy;
    [SerializeField] private int playerLayer;
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.layer == playerLayer)
        {
            enemy.GetComponent<EnemyController>()?.ChasePlayer(collision.transform);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == playerLayer)
        {
            enemy.GetComponent<EnemyController>()?.IdleMyself();
        }
    }
}

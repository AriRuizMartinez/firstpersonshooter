using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{

    private enum SwitchMachineStates { NONE, IDLE, CHASE, ATTACK, PATROL, DIE };
    private SwitchMachineStates m_CurrentState;
    private Transform m_PlayerTransform;
    private NavMeshAgent m_Agent;
    private Coroutine m_CurrentCoroutine;
    [SerializeField] private LayerMask m_ShootMask;
    [SerializeField] private Vector3[] waypoints;
    [SerializeField] private float m_Cadencia;
    [SerializeField] private int m_Probability;
    public Vector3[] Waypoints
    {
        get
        {
            return waypoints;
        }

        set
        {
            waypoints = value;
        }
    }
    private int m_NumWaypoints;

    private void ChangeState(SwitchMachineStates newState)
    {

        if (newState == m_CurrentState)
            return;
        if (m_CurrentState == SwitchMachineStates.DIE)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                m_Agent.velocity = Vector2.zero;
                m_CurrentCoroutine = StartCoroutine(WaitingForPlayer());
                m_Animator.Play("idle_m_2_220f");
                break;

            case SwitchMachineStates.CHASE:
                m_Agent.SetDestination(m_PlayerTransform.position);
                m_Animator.Play("locom_m_jogging_30f");
                break;

            case SwitchMachineStates.ATTACK:

                m_CurrentCoroutine = StartCoroutine(Attack());
                m_Animator.Play("idle_phoneTalking_180f");

                break;

            case SwitchMachineStates.PATROL:

                m_Agent.SetDestination((waypoints[m_NumWaypoints]));
                m_Animator.Play("locom_m_basicWalk_30f");

                break;
            case SwitchMachineStates.DIE:
                m_Agent.velocity = Vector2.zero;
                m_Agent.enabled = false;
                break;
            default:
                break;
        }
    }
    private void UpdateState()
    {

        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Agent.velocity = Vector2.zero;
                break;
            case SwitchMachineStates.CHASE:
                m_Agent.SetDestination(m_PlayerTransform.position);
                break;
            case SwitchMachineStates.ATTACK:
                transform.forward = new Vector3(m_PlayerTransform.position.x - transform.position.x, transform.forward.y, m_PlayerTransform.position.z - transform.position.z);
                RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.forward, out hit, 20f, m_ShootMask))
                {
                    Debug.DrawLine(transform.position, hit.point, Color.green, 2f);

                    if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                        m_Agent.velocity = Vector2.zero;
                    else
                        m_Agent.SetDestination(m_PlayerTransform.position);

                }
                break;
            case SwitchMachineStates.PATROL:

                

                if (m_Agent.remainingDistance <= m_SQDistancePerFrame)
                {
                    m_NumWaypoints++;
                    if (m_NumWaypoints >= waypoints.Length)
                    {
                        m_NumWaypoints = 0;
                    }
                    m_Agent.SetDestination((waypoints[m_NumWaypoints]));
                }
                break;
            case SwitchMachineStates.DIE:
                
                break;
            default:
                ChangeState(SwitchMachineStates.IDLE);
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                StopCoroutine(m_CurrentCoroutine);
                break;

            case SwitchMachineStates.CHASE:

                break;

            case SwitchMachineStates.ATTACK:
                StopCoroutine(m_CurrentCoroutine);
                break;
            case SwitchMachineStates.PATROL:

                break;
            case SwitchMachineStates.DIE:
                break;
            default:
                break;
        }
    }

    private Animator m_Animator;
    private float m_SQDistancePerFrame;

    [Header("Enemy Values")]
    [SerializeField]
    private float m_Speed = 1.75f;
    [SerializeField]
    private int m_Damage = 5;

    void Awake()
    {
        m_Animator = GetComponentInParent<Animator>();
        m_Agent = GetComponentInParent<NavMeshAgent>();
        m_Agent.speed = m_Speed;
        m_NumWaypoints = 0;
        m_SQDistancePerFrame = m_Speed * Time.fixedDeltaTime;
        m_SQDistancePerFrame *= m_SQDistancePerFrame;
        InitState(SwitchMachineStates.PATROL);
    }

    public void ChasePlayer(Transform t)
    {
        m_PlayerTransform = t;
        ChangeState(SwitchMachineStates.CHASE);
    }
    public void AttackPlayer()
    {
        ChangeState(SwitchMachineStates.ATTACK);
    }
    public void IdleMyself()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }
    public void ReturnPatrol()
    {
        ChangeState(SwitchMachineStates.PATROL);
    }
    public IEnumerator WaitingForPlayer()
    {
        yield return new WaitForSeconds(5);
        ChangeState(SwitchMachineStates.PATROL);
    }
    private void Update()
    {
        UpdateState();
    }

    public void ChangeStateDie()
    {
        ChangeState(SwitchMachineStates.DIE);
    }

    private IEnumerator Attack()
    {
        while (true)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward + transform.right * (Random.Range(0, m_Probability) / 100) + transform.up * (Random.Range(0, m_Probability) / 100), out hit, 20f, m_ShootMask))
            {
                Debug.Log($"He tocat {hit.collider.gameObject} a la posici {hit.point} amb normal {hit.normal}");
                Debug.DrawLine(transform.position, hit.point, Color.green, 2f);

                if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                    target.Damage(m_Damage);

                if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
                    pushable.Push(transform.forward, 10);
            }
            yield return new WaitForSeconds(m_Cadencia);
        }
    }

}

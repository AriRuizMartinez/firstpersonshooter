using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

public class MisilController : MonoBehaviour
{
    private PlayerController m_Player;
    [SerializeField]
    private float m_Speed;
    [SerializeField]
    private float m_SpeedVertical;
    [SerializeField]
    private float m_PositionY;
    [SerializeField]
    private int m_Damage;
    [SerializeField]
    private float m_RadiusExplosion;
    [SerializeField]
    private float m_RotationSpeed = 180f;

    private Rigidbody m_Rigidbody;

    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;
    private InputAction m_MouseAction;
    private InputActionMap m_CurrentActionMap;

    private Vector2 input;
    public void Init(PlayerController p)
    {
        transform.position = new Vector3(0, m_PositionY, 0);

        transform.forward = -Vector3.up;
        
        m_Rigidbody = GetComponent<Rigidbody>();

        Assert.IsNotNull(m_InputAsset);

        m_Input = Instantiate(m_InputAsset);
        m_CurrentActionMap = m_Input.FindActionMap("Misil");
        m_MovementAction = m_CurrentActionMap.FindAction("Movement");
        m_MouseAction = m_CurrentActionMap.FindAction("Mouse");
        m_CurrentActionMap.FindAction("Sprint").started += Sprint;
        m_CurrentActionMap.FindAction("Sprint").canceled += Sprintnt;
        m_CurrentActionMap.Enable();
        m_Rigidbody.velocity = new Vector3(0, -m_SpeedVertical, 0);
        m_Player = p;
    }

    private void Update()
    {
        transform.Rotate(Vector3.forward * m_MouseAction.ReadValue<Vector2>().x * m_RotationSpeed * Time.deltaTime);
        input = m_MovementAction.ReadValue<Vector2>();
    }
    private void FixedUpdate()
    {
        m_Rigidbody.velocity = (transform.up * input.y + transform.right * input.x).normalized * m_Speed + Vector3.up * m_Rigidbody.velocity.y;
    }
    private void OnCollisionEnter(Collision collision)
    {
        RaycastHit[] hit = Physics.SphereCastAll(transform.position, m_RadiusExplosion, Vector3.up, 1);
        foreach(RaycastHit hitted in hit)
        {
            Debug.Log("Explosion hit: " + hitted.collider.gameObject.name);
            if (hitted.collider.TryGetComponent<IDamageable>(out IDamageable target))
                target.Damage(m_Damage);

            if (hitted.collider.TryGetComponent<IPushable>(out IPushable pushable))
                pushable.Push(transform.forward, 10);
        }
        m_Player.MisilEnded();
        Destroy(gameObject);
    }
    private void Sprint(InputAction.CallbackContext context)
    {
        m_Speed *= 1.5f;
    }
    private void Sprintnt(InputAction.CallbackContext context)
    {
        m_Speed /= 1.5f;
    }
}

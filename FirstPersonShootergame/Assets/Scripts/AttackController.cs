using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackController : MonoBehaviour
{
    [SerializeField] private GameObject enemy;
    [SerializeField] private int playerLayer;
    private void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.layer == playerLayer)
        {
            enemy.GetComponent<EnemyController>()?.AttackPlayer();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == playerLayer)
        {
            enemy.GetComponent<EnemyController>()?.ChasePlayer(other.transform);
        }
    }
}

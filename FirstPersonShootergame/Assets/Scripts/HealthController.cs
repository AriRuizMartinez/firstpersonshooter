using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthController : MonoBehaviour, IDamageable
{
    [SerializeField] private int m_Health;
    public event Action<int> OnDamage;

    public void Damage(int amount)
    {
        Debug.Log("M'han fet " + amount + " de mal. Soc " + gameObject.name);
        m_Health -= amount;
        Debug.Log("Hem queden " + m_Health + " punts de vida");
        if(m_Health < 0)
        {
            if(TryGetComponent<PlayerController>(out PlayerController pla))
            {
                pla.ChangeStateDie();
                SceneManager.LoadScene("GameOver");
            }
        }
    }

}

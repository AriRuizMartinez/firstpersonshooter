using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float m_Speed = 3f;
    [SerializeField]
    private int m_Damage = 5;
    [SerializeField]
    private float m_AngleLimit = 75f;

    [SerializeField]
    private float m_RotationSpeed = 180f;
    private float m_MouseSensitivity = 1f;

    private Rigidbody m_Rigidbody;

    //Camera
    [SerializeField]
    private GameObject m_Camera;
    [SerializeField]
    private GameObject m_Misil;
    [SerializeField]
    private bool m_InvertY = false;

    [SerializeField]
    private LayerMask m_ShootMask;
    private Vector2 m_Movement = Vector2.zero;
    private float m_RotationY = 0f;

    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;
    private InputAction m_MouseAction;
    private InputActionMap m_CurrentActionMap;

    private enum SwitchMachineStates { NONE, IDLE, WALK, WAITING, DIE };
    private SwitchMachineStates m_CurrentState;

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;
        Debug.Log(newState);
        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Rigidbody.velocity = Vector3.zero;
                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.WAITING:
                m_Rigidbody.velocity = Vector3.zero;
                m_Camera.GetComponent<Camera>().enabled = false;
                DisableActionMap();
                break;
            case SwitchMachineStates.DIE:
                Cursor.lockState = CursorLockMode.Confined;
                DisableActionMap();
                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.WAITING:
                m_Camera.GetComponent<Camera>().enabled = true;
                EnableActionMap();
                break;
            case SwitchMachineStates.DIE:
                Cursor.lockState = CursorLockMode.Locked;
                EnableActionMap();
                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                Rotations();
                m_Movement = m_MovementAction.ReadValue<Vector2>();
                if(m_Movement != Vector2.zero)
                    ChangeState(SwitchMachineStates.WALK);
                break;
            case SwitchMachineStates.WALK:
                Rotations();
                m_Movement = m_MovementAction.ReadValue<Vector2>();
                if(m_Movement == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLE);
                break;
            case SwitchMachineStates.WAITING:
                m_Rigidbody.velocity = Vector3.zero;
                break;
            case SwitchMachineStates.DIE:
                break;
            default:
                break;
        }
    }

    private void Rotations()
    {
        transform.Rotate(Vector3.up * m_MouseAction.ReadValue<Vector2>().x * m_RotationSpeed * Time.deltaTime);
        float mouseY = -m_MouseAction.ReadValue<Vector2>().y * m_RotationSpeed * Time.deltaTime;
        float angles = m_Camera.transform.localEulerAngles.x + mouseY;
        m_RotationY = Mathf.Clamp(angles, -m_AngleLimit, m_AngleLimit);
        m_Camera.transform.localEulerAngles = Vector3.right * angles;
    }

    private void Awake()
    {

        Cursor.lockState = CursorLockMode.Locked;

        m_Rigidbody = GetComponent<Rigidbody>();

        Assert.IsNotNull(m_InputAsset);

        m_Input = Instantiate(m_InputAsset);
        m_CurrentActionMap = m_Input.FindActionMap("Pelea");
        m_MovementAction = m_CurrentActionMap.FindAction("Move");
        m_MouseAction = m_CurrentActionMap.FindAction("Camara");
        EnableActionMap();
    }

    private void Start()
    {
        InitState(SwitchMachineStates.IDLE);
    }

    void Update()
    {
        UpdateState();
    }

    private void FixedUpdate()
    {
        m_Rigidbody.velocity = (transform.forward * m_Movement.y + transform.right * m_Movement.x).normalized * m_Speed + Vector3.up * m_Rigidbody.velocity.y;
    }
    private void Escopeta(InputAction.CallbackContext context)
    {
        RaycastHit hit;
        for (int i = 0; i < 8; i++)
        {
            if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward + m_Camera.transform.right * Random.Range(0, 0.25f) + m_Camera.transform.up * Random.Range(0, 0.25f), out hit, 20f, m_ShootMask))
            {
                Debug.Log($"He tocat {hit.collider.gameObject} a la posici {hit.point} amb normal {hit.normal}");
                Debug.DrawLine(m_Camera.transform.position, hit.point, Color.green, 2f);

                if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                    target.Damage(m_Damage);

                if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
                    pushable.Push(m_Camera.transform.forward, 10);
            }
        }
    }
    private void Shoot(InputAction.CallbackContext context)
    {
        RaycastHit hit;
        if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, 20f, m_ShootMask))
        {
            Debug.Log($"He tocat {hit.collider.gameObject} a la posici {hit.point} amb normal {hit.normal}");
            Debug.DrawLine(m_Camera.transform.position, hit.point, Color.green, 2f);

            if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                target.Damage(m_Damage);

            if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
                pushable.Push(m_Camera.transform.forward, 10);
        }
    }

    private void Misil(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.WAITING);
        GameObject misil = Instantiate(m_Misil);
        misil.GetComponent<MisilController>().Init(this);
    }
    private void Sprint(InputAction.CallbackContext context)
    {
        m_Speed *= 1.5f;
    }
    private void Sprintnt(InputAction.CallbackContext context)
    {
        m_Speed /= 1.5f;
    }
    public void MisilEnded()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }

    public void ChangeStateDie()
    {
        ChangeState(SwitchMachineStates.DIE);
    }

    private void DisableActionMap()
    {
        m_CurrentActionMap.FindAction("Disparar").performed -= Shoot;
        m_CurrentActionMap.FindAction("Escopeta").performed -= Escopeta;
        m_CurrentActionMap.FindAction("Sprint").started -= Sprint;
        m_CurrentActionMap.FindAction("Sprint").canceled -= Sprintnt;
        m_CurrentActionMap.FindAction("Misil").performed -= Misil;
        m_CurrentActionMap.Disable();
    }

    private void EnableActionMap()
    {
        m_CurrentActionMap.FindAction("Disparar").performed += Shoot;
        m_CurrentActionMap.FindAction("Escopeta").performed += Escopeta;
        m_CurrentActionMap.FindAction("Sprint").started += Sprint;
        m_CurrentActionMap.FindAction("Sprint").canceled += Sprintnt;
        m_CurrentActionMap.FindAction("Misil").performed += Misil;
        m_CurrentActionMap.Enable();
    }
}

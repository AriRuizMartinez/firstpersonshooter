using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollController : MonoBehaviour
{
    [SerializeField] private int m_HP = 100;

    [SerializeField]
    private GameObject m_RigRoot;
    private Rigidbody[] m_Bones;
    private Animator m_Animator;
    private EnemyController m_EnemyController;

    private void Awake()
    {
        m_Animator = GetComponentInParent<Animator>();
        m_EnemyController = GetComponent<EnemyController>();
        m_Bones = m_RigRoot.GetComponentsInChildren<Rigidbody>();
        Activate(false);

        foreach (Rigidbody bone in m_Bones)
            if (bone.TryGetComponent<IDamageable>(out IDamageable damageable))
                damageable.OnDamage += ReceiveDamage;
    }

    public void Activate(bool state)
    {
        foreach (Rigidbody bone in m_Bones)
            bone.isKinematic = !state;
        m_Animator.enabled = !state;
    }

    private void ReceiveDamage(int damage)
    {
        m_HP -= damage;
        Debug.Log($"Hem queden {m_HP} punts de vida");
        if (m_HP <= 0)
            Die();
    }

    private void Die()
    {
        foreach (Rigidbody bone in m_Bones)
            if (bone.TryGetComponent<IDamageable>(out IDamageable damageable))
                damageable.OnDamage -= ReceiveDamage;

        Activate(true);
        m_EnemyController.ChangeStateDie();
    }
}
